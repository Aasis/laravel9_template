@extends("admin.layouts.layout")
@section("page_title", "Add Admin")

@section("content")
<section class="content mt-4">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary">
					<div class="card-header">
				 		<h3 class="card-title">Create Admin</h3>
				 		<a href="{{ route('admin.admin') }}" class="card-title float-right" title="Admin List">
				 			<i class="fas fa-list"></i>
				 			View List
				 		</a>
					</div>
					<form id="quickForm" novalidate="novalidate">
						<div class="card-body row">
							<div class="form-group col-md-6">
								<label for="exampleInputEmail1">Email address</label>
								<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
							</div>
							<div class="form-group col-md-6">
								<label for="exampleInputPassword1">Password</label>
								<input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">nbvv</div>
									<div class="col-md-6">mjg</div>
								</div>
							</div>
						</div>

						<div class="card-footer text-center">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection