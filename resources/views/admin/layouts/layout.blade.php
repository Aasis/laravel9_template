@include("admin.includes.header")

@include("admin.includes.nav-bar")

@include('admin.includes.side-bar')

    <div class="content-wrapper">

        @yield('content')

    </div>

@include('admin.includes.footer')