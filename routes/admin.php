<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\{AdminLoginController, AdminForgotPasswordController, AdminResetPasswordController,
    PermissionController};
use App\Http\Controllers\Admin\{AdminController, DashboardController, UserController};


Route::group([], function($route){

    $route->group(["middleware" => "guest:admin"], function($route){

        $route->view("/login", "admin.auth.login")
        ->name("login");
        
        $route->post("/login", [AdminLoginController::class, "loginProcess"])
        ->name('loginProcess');

        $route->view("/password/reset", "admin.auth.email")
        ->name("showLinkRequestForm");

        $route->post("/password/email", [AdminForgotPasswordController::class, "sendResetLinkEmail"])
        ->name("sendResetLinkEmail");

        $route->get("/password/reset/{token}", [AdminResetPasswordController::class, "showResetForm"])
        ->name("showResetForm");

        $route->post("/password/reset", [AdminResetPasswordController::class, "reset"])
        ->name("reset");

    });

    $route->group(["middleware" => "auth:admin"], function($route){

        $route->controller(AdminLoginController::class)->group(function($route){

            $route->post("logout", "logout")->name("logout");

        });

        
        $route->controller(AdminController::class)->group(function($route){

            $route->get("/admin", "index")->name("admin");
            $route->view("/admin/add", "admin.pages.admin.add")->name("admin.add");
            $route->post("/admin/add", "create")->name("admin.create");
            $route->get("/admin/edit/{id}", "edit")->name("admin.edit");
            $route->get("/admin/edit/{id}", "update")->name("admin.update");
            $route->get("/admin/delete/{id}", "delete")->name("admin.delete");

        });


        $route->controller(DashboardController::class)->group(function($route){

            $route->get("/", "dashboard")->name("dashboard");

        });


        $route->controller(UserController::class)->group(function($route){

            $route->get("/user", "index")->name("user");
            $route->view("/user/add", "admin.pages.user.add")->name("user.add");
            $route->post("/user/add", "create")->name("user.create");
            $route->get("/user/edit/{id}", "edit")->name("user.edit");
            $route->get("/user/edit/{id}", "update")->name("user.update");
            $route->get("/user/delete/{id}", "delete")->name("user.delete");

        });


    });

});
