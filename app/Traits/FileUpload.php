<?php 

namespace App\Traits;
use Illuminate\Support\Str;
use Image;
use File;

trait FileUpload {


	public static function createDirectory($path){
		if(!File::exists($path)){ File::makeDirectory($path); }
	}


	public function upload($file, $path, $width=""){

		$pics = Image::make($file);

		($width != "") ? $pics->widen($width) : "";

		$file_name = "this-is-image-".strtolower(Str::random(30)).'.'.$file->extension();

        $pics->save($path.$file_name);

        return $file_name;

	}


	public static function uploadImageThumb($file, $path){

		self::createDirectory($path);

		return self::upload($file, $path, 225);

	}


	public static function multipleImageUpload($files, $path){

		$images = [];

		if($files == null || !isset($files)) return $images;

		foreach($files??[] as $file):

			try{

				$file_name = self::upload($file, $path);

		        array_push($images, $file_name);

			}catch(Exception $error){

				foreach($images??[] as $image):
					@unlink($path.$image);
				endforeach;

			}

		endforeach;

        return $images;

	}


	public static function moveFile($file, $path){

		self::createDirectory($path);

		$file_name = 'file-'.strtolower(Str::random(10)).'.'.$file->extension();

		$file->move($path, $file_name);

        return $file_name;

	}


}


