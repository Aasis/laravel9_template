<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Notifications\AdminResetPasswordNotification;


class Admin extends Authenticatable{

    use HasFactory, Notifiable, HasUuids;
    

    protected $table = "admins";

    protected $fillable = ["name", "email", "password", "profile", "contact", "address", "status"];

    protected $timestamp = true;

    protected $hidden = ["password", "remember_token"];


    public function sendPasswordResetNotification($token){
        $this->notify(new AdminResetPasswordNotification($token));
    }

}
