<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware{


    protected $admin_login = "admin/login";
    protected $user_login  = "user/login";
    protected $index  = "/";


    protected function redirectTo($request){

        if($request->expectsJson())
            return response()->json(['error' => 'Unauthenticated.'], 401);

        if($request->is("admin") || $request->is("admin/*"))
            return $this->admin_login;

        elseif($request->is("web") || $request->is("user/*"))
            return $this->index;

        else
            return $this->index;

    }


}
