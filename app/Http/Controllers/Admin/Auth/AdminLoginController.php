<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\{Admin};


class AdminLoginController extends Controller{

    use AuthenticatesUsers;

    protected $redirectTo = RouteServiceProvider::ADMIN_DASH;


    public function showLoginForm(){
        return view("admin.auth.login");
    }


    public function loginProcess(Request $request){

        $request->validate([
                                "email" => "required|email|exists:admins|max:191",
                                "password" => "required|max:191"
                            ]);

        try{

            if(!auth("admin")->attempt($request->only(["email", "password"]))){
                return redirect(route("admin.login"))
                                ->withErrors(["email" => "Invalid Email or Password ! ! !"])
                                ->withInput($request->only(["email"]));
            }

            return to_route("admin.dashboard");

        }catch(\Exception){

            return redirect(route("admin.login"))
                        ->withErrors(["email" => $error->getMessage()])
                        ->withInput($request->only(["email"]));

        }

    }



    public function changePassword(Request $request){

        $request->validate([
                                "old_password" => "required|min:6|max:191",
                                "new_password" => "required|same:confirm_password|min:6|max:191"
                            ]);

        try{

            $auth = auth("admin")->user();

            // if(Hash::check('passwordToCheck', $user->password)){}

            if(password_verify($request->old_password, $auth->password)):

                $auth->update(["password" => bcrypt($request->new_password)]);
                
                // auth("admin")->logoutOtherDevices($request->old_password); 

                return $this->jsonResponse(true, "Password Changed Successfully ! ! !", 201);

            endif;  

            return $this->jsonResponse(false, "old and New Password do not Matched ! ! !", 400);

        }catch(\Exception $error){
            return $this->jsonResponse(false, $error->getMessage(), 400);
        }

    }



    public function logout(Request $request){

        try{

            auth("admin")->logout();

            return redirect(route("admin.login"));

        }catch(\Exception $error){
            return redirect(route("admin.dashboard"));
        }

    }

}
