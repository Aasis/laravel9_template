<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Admin};
use App\Traits\{FileUpload,Validation};


class AdminController extends Controller{

    use FileUpload, Validation;


    public function index(){

        $data["admins"] = Admin::select(["id", "name", "profile", "status", "created_at"])
                                ->latest()->get();

        return view("admin.pages.admin.index", $data);

    }


    public function store(Request $request){

        
        
    }



}
