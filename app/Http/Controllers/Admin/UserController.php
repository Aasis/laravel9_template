<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{User};
use App\Traits\{FileUpload,Validation};


class UserController extends Controller{

    use FileUpload, Validation;


    public function index(){

        $data["users"] = User::select(["id", "name", "created_at"])
                                ->latest()->get();

        return view("admin.pages.user.index", $data);

    }



}
