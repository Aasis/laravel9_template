<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration{

    public function up(){

        Schema::create('admins', function(Blueprint $table){
            $table->uuid("id");
            $table->string("name");
            $table->string("email")->unique();
            $table->string("password");
            $table->string("profile")->nullable();
            $table->string("contact")->nullable();
            $table->string("address")->nullable();
            $table->enum("status", ["Active", "Banned"])->default("Banned");
            $table->rememberToken();
            $table->timestamps();
        });

    }

    public function down(){
        Schema::dropIfExists('admins');
    }
};
